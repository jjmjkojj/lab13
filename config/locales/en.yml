# Files in the config/locales directory are used for internationalization
# and are automatically loaded by Rails. If you want to use locales other
# than English, add the necessary files in this directory.
#
# To use the locales, use `I18n.t`:
#
#     I18n.t 'hello'
#
# In views, this is aliased to just `t`:
#
#     <%= t('hello') %>
#
# To use a different locale, set it with `I18n.locale`:
#
#     I18n.locale = :es
#
# This would use the information in config/locales/es.yml.
#
# To learn more, please read the Rails Internationalization guide
# available at http://guides.rubyonrails.org/i18n.html.

en:
  app_title: Book Entry
  menu:
    book: Book
    author: Author
    about: About
  button:
    back: Back
    cancel: Cancel
    clear: Clear
  will_paginate:
    previous_label: "&#8592; Previous"
    next_label: "Next &#8594;"
    page_gap: "&hellip;"

    page_entries_info:
      single_page:
        zero:  "No %{model} found"
        one:   "Displaying 1 %{model}"
        other: "Displaying all %{count} %{model}"
      single_page_html:
        zero:  "No %{model} found"
        one:   "Displaying <b>1</b> %{model}"
        other: "Displaying <b>all&nbsp;%{count}</b> %{model}"

      multi_page: "Displaying %{model} %{from} - %{to} of %{count} in total"
      multi_page_html: "Displaying %{model} <b>%{from}&nbsp;-&nbsp;%{to}</b> of <b>%{count}</b> in total"
  helpers:
    submit:
      create: Add %{model}
      submit: Save %{model}
      update: Update %{model}
    select:
      prompt: Please select
  date:
    formats:
      default: ! '%d %b %y'
      long: ! '%B %d, %Y'
      short: ! '%d %b %y'
    abbr_day_names:
    - Sun
    - Mon
    - Tue
    - Wed
    - Thu
    - Fri
    - Sat
    abbr_month_names:
    -
    - Jan
    - Feb
    - Mar
    - Apr
    - May
    - Jun
    - Jul
    - Aug
    - Sep
    - Oct
    - Nov
    - Dec
    day_names:
    - Sunday
    - Monday
    - Tuesday
    - Wednesday
    - Thursday
    - Friday
    - Saturday
    month_names:
    -
    - January
    - February
    - March
    - April
    - May
    - June
    - July
    - August
    - September
    - October
    - November
    - December
    order:
    - :year
    - :month
    - :day
  datetime:
    distance_in_words:
      about_x_hours:
        one: about 1 hour
        other: about %{count} hours
      about_x_months:
        one: about 1 month
        other: about %{count} months
      about_x_years:
        one: about 1 year
        other: about %{count} years
      almost_x_years:
        one: almost 1 year
        other: almost %{count} years
      half_a_minute: half a minute
      less_than_x_minutes:
        one: less than a minute
        other: less than %{count} minutes
      less_than_x_seconds:
        one: less than 1 second
        other: less than %{count} seconds
      over_x_years:
        one: over 1 year
        other: over %{count} years
      x_days:
        one: 1 day
        other: ! '%{count} days'
      x_minutes:
        one: 1 minute
        other: ! '%{count} minutes'
      x_months:
        one: 1 month
        other: ! '%{count} months'
      x_seconds:
        one: 1 second
        other: ! '%{count} seconds'
    prompts:
      day: Day
      hour: Hour
      minute: Minute
      month: Month
      second: Seconds
      year: Year
  time:
    am: am
    formats:
      default: ! '%a, %d %b %Y %H:%M:%S %z'
      long: ! '%B %d, %Y %H:%M'
      short: ! '%d %b %H:%M'
    pm: pm
  messages:
    success:
      create: 'Book was successfully added.'
      update: 'Book was successfully updated.'
      delete: 'Book was successfully deleted.'
  errors: &errors
    models:
      book:
        attributes:
          title:
            blank: must be entered.
          author:
            blank: " of the book is missing."
    format: ! '%{attribute} %{message}'
    messages:
      accepted: must be accepted
      blank: can't be blank
      present: must be blank
      confirmation: ! "doesn't match %{attribute}"
      empty: can't be empty
      equal_to: must be equal to %{count}
      even: must be even
      exclusion: is reserved
      greater_than: must be greater than %{count}
      greater_than_or_equal_to: must be greater than or equal to %{count}
      inclusion: is not included in the list
      invalid: is invalid
      less_than: must be less than %{count}
      less_than_or_equal_to: must be less than or equal to %{count}
      not_a_number: is not a number
      not_an_integer: must be an integer
      odd: must be odd
      record_invalid: ! 'Validation failed: %{errors}'
      restrict_dependent_destroy:
        one: "Cannot delete record because a dependent %{record} exists"
        many: "Cannot delete record because dependent %{record} exist"
      taken: has already been taken
      too_long:
        one: is too long (maximum is 1 character)
        other: is too long (maximum is %{count} characters)
      too_short:
        one: is too short (minimum is 1 character)
        other: is too short (minimum is %{count} characters)
      wrong_length:
        one: is the wrong length (should be 1 character)
        other: is the wrong length (should be %{count} characters)
      other_than: "must be other than %{count}"
    template:
      body: ! 'There were problems with the following fields:'
      header:
        one: 1 error prohibited this %{model} from being saved
        other: "%{count} errors prohibited this %{model} from being saved"
  # remove these aliases after 'activemodel' and 'activerecord' namespaces are removed from Rails repository
  activemodel:
    errors:
      <<: *errors
  activerecord:
    models:
      book: Book
    attributes:
      book:
        title: Book Title
        author: Author
        released: Released
        rating: Rating
        available: Avail.
        actions: U/D
    errors:
      <<: *errors
  number:
    currency:
      format:
        delimiter: ! ','
        format: ! '%u%n'
        precision: 2
        separator: .
        significant: false
        strip_insignificant_zeros: false
        unit: $
    format:
      delimiter: ! ','
      precision: 3
      separator: .
      significant: false
      strip_insignificant_zeros: false
    human:
      decimal_units:
        format: ! '%n %u'
        units:
          billion: Billion
          million: Million
          quadrillion: Quadrillion
          thousand: Thousand
          trillion: Trillion
          unit: ''
      format:
        delimiter: ''
        precision: 3
        significant: true
        strip_insignificant_zeros: true
      storage_units:
        format: ! '%n %u'
        units:
          byte:
            one: Byte
            other: Bytes
          gb: GB
          kb: KB
          mb: MB
          tb: TB
    percentage:
      format:
        delimiter: ''
        format: "%n%"
    precision:
      format:
        delimiter: ''
  support:
    array:
      last_word_connector: ! ', and '
      two_words_connector: ! ' and '
      words_connector: ! ', '
class Author < ActiveRecord::Base
  has_many :books

  def self.find_books(id)
    author = where(id: id).take
    if author
      author.books
    else
      []
    end
  end
end
